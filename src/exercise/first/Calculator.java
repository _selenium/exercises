package exercise.first;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Calculator {

	
	/*3) Follow these steps and test functionality of a calculator:
		a) Go to www.google.com and search for 'calculator'. In the results you will see a calculator coming in the browser itself.
		b) Download this xls from here
		c) Read the Num1, Num2, Operation and ExpectedResult columns using java code
		d) Perform the operation on the calculator as described in xls file
		e) Write the value in ExpectedResult column as result of actual operation on the application
		f) Write the value in Result coulmn as Pass/Fail depending on output
*/
	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;
	public static int rowCount;
	public static int colCount;
	public static String num1,num2;
	public static void main(String[] args) {
		//FileInputStream fis= new FileInputStream(System.getProperty("user.dir")+"\\config\\Calculator.xlsx");
		FileInputStream fis;
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+"\\config\\Calculator.xlsx");
			workbook=new XSSFWorkbook(fis);
			sheet=workbook.getSheet("Addition");
			XSSFRow firstRow=sheet.getRow(1);
			XSSFCell cellNum1=firstRow.getCell(0);
			XSSFCell cellNum2=firstRow.getCell(1);
			rowCount=getRowcount("Addition");
			colCount=firstRow.getLastCellNum();
			System.out.println(getRowcount("Addition"));
			
			for(int j=0;j<=rowCount;j++){
				for(int i=0;i<=colCount;i++){
				num1=firstRow.getCell(i).getStringCellValue();
				System.out.println(num1);
				}

			}
			

		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		catch(IOException ex){
			ex.printStackTrace();
		}
		
		

	}
	public static int getRowcount(String sheetName){
		int index=workbook.getSheetIndex(sheetName);
		if (index==-1)
		return 0;
		else{
			sheet=workbook.getSheetAt(index);
			int number=sheet.getLastRowNum()+1;
			return number;
			
		}
	}

}
