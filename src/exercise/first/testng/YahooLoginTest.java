package exercise.first.testng;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class YahooLoginTest {
	
  @Test(dataProvider="registerData")
  public void LoginTest(String username,String password,String email,String cityname) {
	  System.out.println("Login Test executing");
	  System.out.println(username+"------"+password+"------"+email+"-----"+cityname);
  }
  
  
  
  @Test
  public void  assertTest1(){
	//  Assert.assertEquals("Good", "expected");
	  Assert.assertTrue(5<10, "statment condiotion true");
	  Assert.assertFalse(5>10, "statment condiotion true");

  }
  
  @DataProvider
  public Object[][] registerData(){
	  
	  Object [][] data=new Object[3][4];
	 
	  data[0][0]="user1";
	  data[0][1]="pass1";
	  data[0][2]="email1";
	  data[0][3]="city1";
	  
	  data[1][0]="user2";
	  data[1][1]="pass2";
	  data[1][2]="email2";
	  data[1][3]="city2";

	  data[2][0]="user3";
	  data[2][1]="pass3";
	  data[2][2]="email3";
	  data[2][3]="city3";

//	  data[3][0]="user4";
//	  data[3][1]="pass4";
//	  data[3][2]="email4";
//	  data[3][3]="city4";
//
//	  data[4][0]="user5";
//	  data[4][1]="pass5";
//	  data[4][2]="email5";
//	  data[4][3]="city5";
//
//	  data[5][0]="user6";
//	  data[5][1]="pass6";
//	  data[5][2]="email6";
//	  data[5][3]="city6";

	  return data;
	  
	  
	  
  }
  
}
