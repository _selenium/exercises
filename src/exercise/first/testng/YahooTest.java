package exercise.first.testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class YahooTest {
  @Test
  public void testingRecevingMail() {
	  System.out.println("Test  Receving mail executing ");
  }
  
  @Test
  public void sendingMail(){
	  
	  System.out.println("Test Sending mail executing ");
	  throw new SkipException("Skipping test due to some reason");
  }
  
  @BeforeMethod
  public void beforeMethod(){
	  System.out.println("Before method executing..");
  }
  
  
  @AfterMethod
  public void afterMethod(){
	  System.out.println("after  method executing..");

  }
  @BeforeTest
  public void beforeTest() {
	  
	  System.out.println("before Test executing..");
  }

  @AfterTest
  public void afterTest() {
	  
	  System.out.println("after test executing  ...");
  }
  
  @BeforeClass
  public void beforeClass(){
	  System.out.println("Before clas  executing");

  }
  
  
  @AfterClass
  public void afterClass(){
	  System.out.println("After clas  executing");
	  
  }

}
