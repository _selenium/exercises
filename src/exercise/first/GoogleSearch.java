package exercise.first;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleSearch {

	
	/*
	 * 2) Go to www.google.com and search for something.
a) Print names of all result links on first page.
b) Print names of all result links on first 5 pages
c) Click on each and every link on first 5 pages and check if next page is opening

	 */
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver= new FirefoxDriver();
		driver.navigate().to("https://www.google.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='lst-ib']")).sendKeys("Selenium");
		driver.findElement(By.xpath("//button[@value='Search']")).click();
		List<WebElement> firstpageLinks=driver.findElements(By.tagName("h3"));
		System.out.println(firstpageLinks.size());

		List<WebElement> nextPagelinks=driver.findElements(By.xpath("//table[@id='nav']/tbody/tr/td/a"));
		for(int i=0;i<nextPagelinks.size();i++){
			nextPagelinks.get(i).click();
			Thread.sleep(8000L);
			System.out.println(driver.getTitle());
			firstpageLinks=driver.findElements(By.tagName("h3"));
			System.out.println("********************** Page  "+i+"*********************");
			for(int j=0;j<firstpageLinks.size();j++){
				System.out.println(firstpageLinks.get(j).getText());
			}
			driver.navigate().to("https://www.google.com/?gws_rd=ssl#q=selenium");
			Thread.sleep(5000L);
			nextPagelinks=driver.findElements(By.xpath("//table[@id='nav']/tbody/tr/td/a"));
		
		}
//		for(int i=0;i<firstpageLinks.size();i++){
//			
//			firstpageLinks.get(i).click();
//			Thread.sleep(5000L);
//			System.out.println(driver.getTitle());
//
//			driver.navigate().to("https://www.google.com/?gws_rd=ssl#q=selenium");
//			Thread.sleep(5000L);
//			firstpageLinks=driver.findElements(By.tagName("h3"));
//		}
		

		}

}
