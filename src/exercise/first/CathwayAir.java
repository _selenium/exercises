package exercise.first;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CathwayAir {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\chromedriver\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("http://www.cathaypacific.com/cx/en_US.html");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@id='change-region']")).click();
		List<WebElement> countryList=driver.findElements(By.xpath("//div[@class='regions-wrapper']/ul/li/a"));
		String name="India - English";
		System.out.println(countryList.size());
		
		for(int i=0;i<countryList.size();i++){
			if(countryList.get(i).getText().equalsIgnoreCase(name)){
			System.out.println(countryList.get(i).getText());
			countryList.get(i).click();
			}
			countryList=driver.findElements(By.xpath("//div[@class='regions-wrapper']/ul/li/a"));
		}
		
		//((Javascript) driver)
	}

}
